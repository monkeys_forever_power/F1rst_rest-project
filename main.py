from fastapi import FastAPI, Form, Request
from fastapi.responses import RedirectResponse,Response
from fastapi.templating import Jinja2Templates
from secrets import token_hex
from database import User, SessionLocal
import uvicorn


templates = Jinja2Templates(directory='public')
data_base = SessionLocal()
app = FastAPI()

@app.get('/')
def main():
    return RedirectResponse('/login')

@app.get('/login')
def login(request: Request):
    attention = request.cookies.get('attention')
    if attention == None:
        attention = ''
    return templates.TemplateResponse('index.html', {'request': request,
                                                     'title': 'My first FastApi project',
                                                     'attention': attention})

@app.post('/postdata')
async def postdata(response: Response,username=Form(), password=Form()):
    log_user = data_base.query(User).filter(User.username == username, User.password == password).first()
    if log_user == None:
        response = RedirectResponse('/login',status_code=303)
        response.set_cookie(key='attention', value='Wrong username or password')
        return response
    else:
        response = RedirectResponse('/user', status_code=303)
        response.set_cookie(key='user_token', value=log_user.token)
        response.delete_cookie(key='attention')
        return response

@app.get('/register')
def register(request: Request):
    register_attention = request.cookies.get('register_attention')
    if register_attention == None:
        register_attention = ''
    return templates.TemplateResponse('register_field.html', {'request': request,
                                                              'attention': register_attention})

@app.post('/registration')
async def registration(response: Response, username=Form(), password=Form(), password_repeat=Form()):
    if password == password_repeat:
        response.delete_cookie(key='register_attention')
        token = token_hex(16)
        user = User(username=username, password=password, token=token)
        data_base.add(user)
        data_base.commit()
        response = RedirectResponse('/user', status_code=303)
        response.set_cookie(key='user_token', value=token)
        response.delete_cookie(key='register_attention')
        return response
    else:
        response = RedirectResponse('/register', status_code=303)
        response.set_cookie(key='register_attention', value='passwords don\'t match')
        return response

@app.post('/updateInfo')
def update_info(request: Request, salary=Form(), next_up_date=Form()):
    user_token = request.cookies.get('user_token')
    user = data_base.query(User).filter(User.token == user_token).first()
    user.salary = salary
    user.next_up_date = next_up_date
    data_base.commit()
    return RedirectResponse('/user', status_code=303)

@app.get('/user')
async def show_user(request: Request):
    if request.cookies.get('user_token') != None:
        user_token = request.cookies.get('user_token')
        user = data_base.query(User).filter(User.token == user_token).first()
        return templates.TemplateResponse('user_list.html', {'request': request,
                                                             'username': user.username,
                                                             'token': user.token,
                                                             'salary': user.salary,
                                                             'next_up_date': user.next_up_date
                                                             })
    else:
        return RedirectResponse('/login')


@app.get('/logout')
def logout():
    response = RedirectResponse('/login')
    response.delete_cookie(key='user_token')
    response.delete_cookie(key='attention')
    response.delete_cookie(key='register_attention')
    return response


if __name__ == '__main__':
    uvicorn.run('main.py', host='127.0.0.1', port=8000, reload=True)